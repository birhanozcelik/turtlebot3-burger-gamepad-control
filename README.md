
# Gamepad Kullanım Adımları

  - burger-control.html dosyasını herhangi bir tarayıcıda aç.
  - Riders'ta editörün navbarına sol tıklayıp "öğeyi denetle"

![GitHub Logo](/images/1.png)

   - console'da şu komutu çalıştır 

```sh
$ document.getElementsByTagName('iframe')[0].src
```


![GitHub Logo](/images/image2.png)
    
- Çıkan url'de "https" sil yerine "wss" yaz
- 8888'i 9090 ile değiştir
- url'in sonundaki "/#/workspace" kısmı sil 
- Bu değişikliklerden sonra elde ettiğin url'i input alanına gir ve "Connect to Riders" butonuna bas.
![GitHub Logo](/images/image3.png)
- Riders'da terminal açıp aşağıdaki komutu çalıştır
```sh
$ rostopic list
```
![GitHub Logo](/images/topicList.png)
- Ekranda cmd_vel topic'ini göreceksin
![GitHub Logo](/images/topicList.png)
- Verileri görebilmek için aşağıdaki komutu çalıştır
```sh
$ rostopic echo cmd_vel
```
![GitHub Logo](/images/image4.png)
- Robotu hareket ettirebilmek için Joystick'leri hareket ettir. Joystick'leri hareket ettirdiğin de linear.x ve angular.z değerlerinin değişimini gözlemleyebilirsin.


